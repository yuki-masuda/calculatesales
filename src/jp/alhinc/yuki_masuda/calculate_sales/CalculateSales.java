package jp.alhinc.yuki_masuda.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	//支店定義ファイルの読み込み
	public static void main(String[]args){
		System.out.println("ここにあるファイルを開きます=>"+ args[0]);

		 //branch.lstのmap
        Map<String,String> branchMap = new HashMap<>();

		//売上合算用のmap
		Map<String,Long> salesMap = new HashMap<>();

        BufferedReader br = null;
        try {
        	File file = new File(args[0],"branch.lst");
    		FileReader fr = new FileReader(file);
    		br = new BufferedReader (fr);
    		String line;
    		boolean fileExists = file != null;
    		if(!(fileExists)) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}
    		while((line = br.readLine()) != null) {
    			System.out.println(line);

    			String[] split = line.split(",");

    			//支店定義ファイルのフォーマットが不正
    			if(split.length == 2 || !split[0].matches("^[0-9]{3}$")) {
    				//配列のサイズが2以外、または、店舗番号が数字3桁でない場合
    				System.out.println("支店定義ファイルのフォーマットが不正です");
    				return;
    			}
    			//putする
				branchMap.put(split[0], split[1]);
				salesMap.put(split[0], 0L);
    	     }

        }catch(IOException e) {
        	System.out.println("予期せぬエラーが発生しました");
        	return;
		}finally {
			if(br != null) {
				try {
					br.close();
				}catch(IOException e) {

				}
            }
		}

        //ファイルの検索・処理
		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File file,String str) {
				if(str.matches("^[0-9]{8}.rcd$")) {
					return true;
				}else{
					return false;
				}
			}
		};

        File[] files = new File(args[0]).listFiles(filter);
        	for (int i=0; i<files.length; ++i) {
        		BufferedReader br2 = null;
        		try {
        			FileReader fr2 = new FileReader(files[i]);
        			br2 = new BufferedReader(fr2);
        			String sale;
        			List<String> salesDataList = new ArrayList<>();
        			while ((sale = br2.readLine()) != null) {
        				salesDataList.add(sale);
        			}

        		    //フォーマットチェック。ファイルが3行以上ならエラー
        			if(salesDataList.size() >= 3) {
        				System.out.println("<" + files[i] + ">のフォーマットが不正です");
        				return;
        			}

        		    //支店コードチェック
        			if(!(branchMap.containsKey(salesDataList.get(0)))) {
        				System.out.println("<" + files[i] + ">の支店コードが不正です");
        				return;
        			}

        		    //合算
        			Long data = Long.parseLong(salesDataList.get(1));
        			Long sum = salesMap.get(salesDataList.get(0));
        			if(sum == null) {
        				//putする
        				salesMap.put(salesDataList.get(0), data);
        			}else {
        				sum += data;
        			}

    		        //桁数チェック
        			if(String.valueOf(sum).length() > 10) {
        				System.out.println("合計金額が10桁を超えました");
        				return;
        			}
        			//putする
        			salesMap.put(salesDataList.get(0), sum);

        		}catch(IOException e) {

        		}finally {
        			if(br2 != null) {
        				try {
        					br2.close();
        				}catch(IOException e) {
        					System.out.println("予期せぬエラーが発生しました");
        					return;
        				}
        			}
        		}
        	}
        //支店別集計ファイル出力
        try {
        	File file = new File(args[0],"branch.out");
            FileWriter fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);
            for(Map.Entry<String, String> entry : branchMap.entrySet()) {
            	bw.write(entry.getKey() +","+ entry.getValue() +","+ salesMap.get(entry.getKey())+"\r\n");
        	}
        	bw.close();
        }catch(IOException e) {

        }
	}
}
